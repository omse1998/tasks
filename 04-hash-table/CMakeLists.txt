cmake_minimum_required(VERSION 2.8)
project(concurrent-hash-map)

if (TEST_SOLUTION)
  include_directories(../private/hash-table)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)
  
endif()

add_gtest(test_hash_map test.cpp)

add_benchmark(bench_hash_map run.cpp)
