#include <benchmark/benchmark.h>
#include <benchmark/benchmark_api.h>
#include <immutable_vector.h>

#include <vector>
#include <thread>
#include <mutex>
#include <random>
#include <algorithm>

void make_transactions(benchmark::State& state, int threads_count) {
    const int kIterationsCount = 100000;
    std::vector<std::thread> threads;
    threads.reserve(threads_count);
    ImmutableVector<int> data;
    std::mutex mutex;
    for (int i = 0; i < threads_count; ++i)
        threads.emplace_back([&data, &mutex, kIterationsCount, i]() {
            std::mt19937 gen(93475 + i);
            mutex.lock();
            auto my_data = data;
            mutex.unlock();
            std::vector<int> indices(kIterationsCount);
            for (int i = 0; i < kIterationsCount; ++i) {
                my_data = my_data.push_back(i);
                indices[i] = i;
            }
            std::shuffle(indices.begin(), indices.end(), gen);
            for (int i = 0; i < kIterationsCount / 2; ++i)
                my_data = my_data.set(indices[i], -1);

            mutex.lock();
            data = my_data;
            mutex.unlock();
        });

    for (auto& thread : threads)
        thread.join();

    int count = 0;
    for (size_t i = 0; i < data.size(); ++i) {
        int cur_value = data.get(i);
        if (cur_value != i && cur_value != -1)
            state.SkipWithError("Bad value of result data");
        if (cur_value == -1)
            ++count;
    }

    if (count != kIterationsCount / 2)
        state.SkipWithError("Wrong -1 count");
}

void run(benchmark::State& state) {
    while (state.KeepRunning()) {
        int threads_count = state.range(0);
        make_transactions(state, threads_count);
    }
}

BENCHMARK(run)->Arg(std::thread::hardware_concurrency())
    ->Unit(benchmark::kMillisecond)->UseRealTime();

BENCHMARK_MAIN();
