# С++ часть 2

_Будет сложно, вам понравится_

---

# Что будет в курсе

## Advanced Concurrency
 * Будем учить _как_ писать многопоточный код
 * Будем заглядывать под капот и ломать абстракции
## Работа с сетью
 * Будем применять знания из части 1
## C++ в задачах ML
 * Покажем как выжать максимум из железа на 1 ядре
## На сдачу
 * Одна лекция про шаблоны
 * Семинары про большие проекты 

---

# Семинары и домашки

 * 12 семинаров
 * 3 домашки
 * Формулу еще не доделали

## Исправляем ошибки прошлого семестра
 - Объясняем в задачах не только "что", но и "как делать"
 - Большие задачи не будут пересекаться по дедлайнам
 - Первая домашка после 2 лекции

---

# [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso)

## Concurrency
 - Способ писать код
 - Композиция независимо работающих процессов
 - FSM, Threads, Coroutines, EventLoop-s

## Parallelism
 - Одновременное выполнение нескольких процессов

---

# Concurrency vs Parallelism
 - Concurrency - про структуру.
 - Parallelism - про исполнение.

---

# Картина мира

 Вам нужно 2 вещи:
  1. Модель в голове.
  2. Инструменты для наблюдения за реальным миром.

---

# Linux
 * Операционная система. _&mdash;Да ну!_
 * Выполняет много задач одновременно (_concurrently_)

---

# Linux
 * Выполняет браузер
 * Обрабатывает wifi
 * Переключает процессы

---
# Linux

## Есть всего две базовых абстракции
 * Процесс - долгая задача, может подождать
 * Прерывание - маленькая срочная задача, возникает асинхронно

---
# Процессы и прерывания - модель в голове
## ⇐ Картинка на доске

---

# Процессы и прерывания - инструменты
  1. `sudo apt-get install htop && sudo htop`. Shift+K.
  2. `cat /proc/interrupts` 

---
# Процессор - модель в голове
## ⇐ 2 картинки на доске!

---
# Процессор - инструменты
  1. `perf record --event 'irq:*' -a && perf script`
  2. `perf record --event 'sched:*' -a && perf script`

---
# Перерыв

---
# And Now Something Completely Different

```c++
#include <thread>

void main() {
    std::thread important_doer([] {
        DoSomethingImportant();
    });
    
    DoSomethingElse();
}
```

---
# And Now Something Completely Different

```c++
#include <thread>

void main() {
    std::thread important_doer([] {
        DoSomethingImportant();
    });
    
    DoSomethingElse();

    important_doer.join();
        
    // Terminate Process
}
```

---
# Как передать данные внутрь thread?

```c++
#include <thread>
#include <vector>

void main() {
    std::vector<int> data = ReadInput();

    std::thread adder([&data] {
        int sum = 0;
        for (int elem : data) sum += elem;
    });
    
    DoSomethingElse();
    
    adder.join();    
}
```

---
# Как вернуть данные из thread?

```c++
#include <thread>
#include <vector>

void Sum(const std::vector<int>& data, int* result) {
    for (int elem : data) *result += elem;
}

void main() {
    std::vector<int> data = ReadInput();

    int result = 0;
    std::thread adder(Sum, std::ref(data), &result);
    
    DoSomethingElse();
    
    summer.join();    
}
```

---
# Как вернуть данные из thread?

```c++
#include <thread>
#include <vector>

void Sum(const std::vector<int>& data, int* result) {
    for (int elem : data) *result += elem;
}

void main() {
    std::vector<int> data1 = ReadInput();
    std::vector<int> data2 = ReadInput();

    int result = 0;
    std::thread adder1(Sum, std::ref(data1), &result);
    std::thread adder2(Sum, std::ref(data2), &result);
    
    adder1.join();
    adder2.join();
}
```

---
# Как доказать что в коде есть ошибка?

```c++
void Sum(const std::vector<int>& data, int* result) {
    for (int elem : data) *result += elem;
}

void main() {
    std::vector<int> data1, data2;

    int result = 0;
    std::thread adder1(Sum, std::ref(data1), &result);
    std::thread adder2(Sum, std::ref(data2), &result);
    
    adder1.join();
    adder2.join();
}
```

---
# Что вообще считать некорректной программой?

```c++
void Set(int* result) {
    *result = 1;
}

void main() {
    std::vector<int> data1, data2;

    int result;
    std::thread setter1(Set, &result);
    std::thread setter2(Set, &result);
    
    setter1.join();
    setter2.join();
}
```

---
# Перерыв

---
# Memory Model
 - MM - это формальная модель выполнения программы
 - Программа - это:
   * Переменные
   * Действия над переменными
 - Каждая переменная имеет своё расположение в памяти
 - Над действиями есть отношение порядка _happens before_

_&mdash; Опять модель? Да сколько можно?_

---
# Memory Model

```c++
void f() {
    int a = 3, b = 4;
    b = 10;
    a = 5;
    std::cout << a << std::endl;
}
```

---
# Memory Model

```c++
void f() {
    int a = 3;
    int *b = &a;
    a = 4;
    *b = 5;
    std::cout << a << std::endl;
}
```

---
# Memory Model - Data Race

**Опр:** Data Race - две или более неатомарных операции обращения к памяти, хотябы одна из которых запись, между которыми нет отношения порядка _happens before_

---
# Есть ли Data Race в коде?

```c++
void Sum(const std::vector<int>& data, int* result) {
    for (int elem : data) *result += elem;
}

void main() {
    std::vector<int> data1, data2;

    int result;
    std::thread adder1(Sum, std::ref(data1), &result);
    std::thread adder2(Sum, std::ref(data2), &result);
    
    adder1.join();
    adder2.join();
}
```

---
# Есть ли Data Race в коде?

```c++
void Set(int* result) {
    *result = 1;
}

void main() {
    std::vector<int> data1, data2;

    int result;
    std::thread setter1(Set, &result);
    std::thread setter2(Set, &result);
    
    setter1.join();
    setter2.join();
}
```

---
# Есть ли Data Race в коде?

```c++
void Set(int* result) {
    *result = 1;
}

void main() {
    std::vector<int> data1, data2;

    int result;
    std::thread setter1(Set, &result);
    setter1.join();
    
    std::thread setter2(Set, &result);
    setter2.join();
}
```

---
# Sequential Consistency for Data Race Free Program
 * Стандарт определяет семантику только для программ не содержащих Data Race
 * Это используют авторы библиотек, компиляторов и процессоров для того чтобы выполнять оптимизации на кодом
 * Более подробно Memory Model будет обcуждаться в 3 лекции

---
# Какие еще есть способы задавать порядок?
 * Выполнение первого действия в новом потоке происходит после вызова конструктора `std::thread`
 * Возврат из `.join()` происходит после выполнения последней операции в потоке

---
# Совсем небольшой перерыв

---
# std::mutex

```c++
class mutex {
public:
    void lock();
    void unlock();
}
```

 * Объект с двумя состояниями LOCKED и UNLOCKED
 * Возврат из `lock()` происходит после соответствуещего вызова `unlock()`
 * Звать `unlock()` в состоянии UNLOCKED нельзя

---
# Есть ли Data Race в коде?

```c++

void Set(std::mutex* mutex, int* result) {
    mutex->lock();
    *result = 1;
    mutex->unlock();
}

void main() {
    std::vector<int> data1, data2;

    int result;
    std::mutex m;
    std::thread setter1(Set, &m, &result);
    std::thread setter2(Set, &m, &result);

    setter2.join();
    setter1.join();
}
```


