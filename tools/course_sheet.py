#!/usr/bin/python3
import httplib2
import os
import re
import argparse
import logging
import time
import pprint
import datetime


from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage


import course_gitlab


SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Shad CPP Bot'
SPREADSHEET_ID = "1DFkY0LygOIP-pFY4HCJiWqPOQBSJSS60mkRpqdNW8Wk"
ENV_CREDENTIALS = "SHAD_GDOC_CREDENTIALS"

logger = logging.getLogger(__name__)

def get_credentials(flags):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

class CourseSheet:
    RANGE_SUBMIT_LOG = "Лог Посылок"
    RANGE_REPO_FORM = "'Заявки на Репозиторий'!A:D"
    RANGE_UPDATE_FORM = "'Заявки на Репозиторий'!D{}"
    RANGE_TASK_HEADER = "'Оценки'!2:2"
    RANGE_STUDENTS = "'Оценки'!A3:C1000"
    RANGE_UPDATE_SCORE = "'Оценки'!{}{}"
    
    def __init__(self, service, spreadsheetId):
        self.service = service
        self.spreadsheetId = spreadsheetId

    def get_task_list(self):
        result = self.service.spreadsheets().values().get(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_TASK_HEADER).execute()
        values = result.get('values', [[]])[0]
        return [int(text) for text in values if re.match("\d+", text)]

    def get_student_list(self):
        result = self.service.spreadsheets().values().get(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_STUDENTS,
            valueRenderOption="FORMULA").execute()
        values = result.get('values')
        return values

    def get_repo_requests(self):
        result = self.service.spreadsheets().values().get(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_REPO_FORM
        ).execute()

        values = result['values'][1:]
        return values

    def set_repo_status(self, index, status):
        self.service.spreadsheets().values().update(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_UPDATE_FORM.format(index + 2),
            valueInputOption="USER_ENTERED",
            body={"values": [[status]]}).execute()

    def add_student(self, name, login, repo_url):
        if len(name) == 0 or re.match("\W", name[0], flags=re.UNICODE):
            raise ValueError("Name looks fishy")
        
        if len(login) == 0 or re.match("\W", login[0], flags=re.UNICODE):
            raise ValueError("Login looks fishy")
        
        self.service.spreadsheets().values().append(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_STUDENTS,
            valueInputOption="USER_ENTERED",
            body={"values": [[
                '=HYPERLINK("{}";"git")'.format(repo_url),
                name,
                login, # Spreadsheet injection, Oh My!
            ]]}).execute()

    def set_score(self, login, task_num, score, overwrite=False):
        students = self.get_student_list()
        for i in range(len(students)):
            # Gitlab converts names to lowercase and replaces '.' with '-'
            if students[i][2].lower().replace('.', '-') == login.lower().replace('.', '-'):
                break
        else:
            raise ValueError("Login {} not found in spreadsheet".format(login))

        tasks = self.get_task_list()
        for j in range(len(tasks)):
            if tasks[j] == task_num:
                break
        else:
            raise ValueError("Task {} not found in spreadsheet".format(task_num))        

        if isinstance(score, float):
            score = ("%.1f" % score).replace(".", ",")

        query_range=CourseSheet.RANGE_UPDATE_SCORE.format(chr(ord('D') + j), 3 + i)
        if not overwrite:
            old = self.service.spreadsheets().values().get(
                spreadsheetId=self.spreadsheetId,
                range=query_range).execute()
            values = old.get("values", [])
            if len(values) > 0 and len(values[0]) > 0:
                return
        
        self.service.spreadsheets().values().update(
            spreadsheetId=self.spreadsheetId,
            range=query_range,
            valueInputOption="USER_ENTERED",
            body={"values": [[score]]}).execute()

    def append_submit(self, time, login, task, commit_url, status):
        result = self.service.spreadsheets().values().append(
            spreadsheetId=self.spreadsheetId,
            range=CourseSheet.RANGE_SUBMIT_LOG,
            valueInputOption="USER_ENTERED",
            body={"values": [[time, login, task, commit_url, status]]}).execute()

def configure_argparser():
    parser = argparse.ArgumentParser(parents=[tools.argparser])
    subparsers = parser.add_subparsers(dest="cmd")
    subparsers.required = True

    check_parser = subparsers.add_parser("check", help="Query data from gdoc, used from manual testing")
    repos_parser = subparsers.add_parser("create_repos", help="Create repositories from google form")
    return parser

def create_repos(sheet):
    gitlab = course_gitlab.get_gitlab()
    requests = sheet.get_repo_requests()
    for i, req in enumerate(requests):
        if len(req) > 3:
            continue

        name = req[1]
        login = req[2]
        try:
            course_gitlab.create_project(gitlab, login)
            sheet.add_student(name, login, "https://gitlab.com/shad-cpp-course/student-" + login)
            sheet.set_repo_status(i, "OK")
        except Exception:
            logger.exception("Can't create project")

        time.sleep(1.0) # More time to press ^C


def check(sheet):
    print("=== Task List ===")
    pprint.pprint(sheet.get_task_list())

    print("=== Student List ===")
    pprint.pprint(sheet.get_student_list())

    print("=== Repo Requests ===")
    pprint.pprint(sheet.get_repo_requests())

    print("=== Testing Form Status Update ===")
    sheet.set_repo_status(0, "OK")

    print("=== Testing Add Student ===")
    sheet.add_student("Короткий Фёдор Михайлович", "slon", "https://gitlab.com/shad-cpp-course/student-slon")

    print("=== Testing Set Score ===")
    sheet.set_score("short-slon", 1, 1.0)
    sheet.set_score("short-slon", 1, 0.3)
    sheet.set_score("short-slon", 2, 0.3)

    print("=== Testing Append Submit ===")
    sheet.append_submit(str(datetime.datetime.now()), "short-slon", "01-hello-world",
                        "https://gitlab.com/shad-cpp-course/student-short-slon/commit/12d29bc64f520393a7296ba1c952b48ee98310a0",
                        "FAIL")


def get_sheet_from_env():
    if ENV_CREDENTIALS not in os.environ:
        raise ValueError(ENV_CREDENTIALS + " not set")
    credentials = client.Credentials.new_from_json(os.environ[ENV_CREDENTIALS])
    http = credentials.authorize(httplib2.Http())

    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    logger.info("API Discovery Finished")

    course = CourseSheet(service, SPREADSHEET_ID)
    return course


def run_flow(flags):
    credentials = get_credentials(flags)
    http = credentials.authorize(httplib2.Http())
    logger.info("Authorized")

    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    logger.info("API Discovery Finished")    

    
def main():
    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', level=logging.ERROR)
    logger.setLevel(logging.INFO)

    parser = configure_argparser()
    flags = parser.parse_args()

    course = get_sheet_from_env()

    if flags.cmd == "check":
        check(course)
    elif flags.cmd == "create_repos":
        create_repos(course)

if __name__ == '__main__':
    main()
