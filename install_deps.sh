#!/bin/sh

set -e -x

apt-get update && apt-get install -y curl

curl -s https://packagecloud.io/install/repositories/shad-cpp/course/script.deb.sh | bash

apt-get update && apt-get install -y \
    gdb \
    build-essential \
    cmake \
    ninja-build \
    clang-format \
    clang-tidy \
    g++ \
    libboost-dev \
    libgtest-dev \
    libbenchmark-dev \
    libsnappy-dev \
    libshad-gtest \
    sudo \
    strace
