import yaml
import os
import sys
import glob
import pytz
import datetime
import pprint

from doit.tools import result_dep


sys.path = ["./tools"] + sys.path
import course_sheet


TZ_MOSCOW = pytz.timezone('Europe/Moscow')


def load_tasks():
    config = yaml.load(open(".deadlines.yml"))
    tasks = {}
    for group in config:
        deadline = TZ_MOSCOW.localize(datetime.datetime.strptime(
            group["deadline"], "%d-%m-%Y %H:%M"))

        for task in group.get("tasks", []):
            config = yaml.load(open(os.path.join(task, ".tester-config.yaml")))

            sources = config.get("allow_change", [])
            if not sources:
                raise ValueError("No sources specified in 'allow_change' for %s" % task)

            for src in sources:
                if not os.path.exists(os.path.join(task, src)):
                    raise ValueError("Source file %s not found in %s" % (src, task))
            
            tasks[task] = {
                "deadline": deadline,
                **config
            }
    return tasks

TASK_LIST = load_tasks()

DOIT_CONFIG = {
    'default_tasks': []
}

def gen_cmake(name, build_type, solution=None):
    task_name = "cmake:{}:{}".format(name, build_type)
    build_dir = os.path.join(name, "build", build_type)
    cmake_cmd = "cmake ../../ -G Ninja -DCMAKE_BUILD_TYPE={}".format(build_type)
    
    if solution is not None:
        task_name += "_" + solution
        build_dir += "_" + solution
        cmake_cmd += " -DTEST_SOLUTION=" + solution
        cmake_cmd += " -DENABLE_PRIVATE_TESTS=1"
        
    return {
        "basename": task_name,
        "targets": [os.path.join(build_dir, "build.ninja")],
        "file_dep": ["dodo.py"],
        "actions": [
            "mkdir -p " + build_dir,
            "cd {} && {}".format(build_dir, cmake_cmd)
        ]
    }


def ninja_task_name(name, build_type):
    return "ninja:{}:{}".format(name, build_type)


def run_ninja(name, build_type, config, solution=None):
    task_name = ninja_task_name(name, build_type)
    build_dir = os.path.join(name, "build", build_type)

    binaries = [config["gtests"]]
    if 'benchmarks' in config:
        binaries.append(config['benchmarks'])
    
    if solution is not None:
        task_name += "_" + solution
        build_dir += "_" + solution

    return {
        "basename": task_name,
        "targets": list({os.path.join(build_dir, b) for b in binaries}),
        "file_dep": [os.path.join(build_dir, "build.ninja")],
        "actions": ["ninja -v -C " + build_dir],
        "uptodate": [False]
    }


def gtest_task_name(name, binary_name):
    return "gtest:{}:{}".format(name, binary_name)


def run_gtest(name, binary_name, no_mem_limit=False):
    task_name = gtest_task_name(name, binary_name)
    builds_dir = os.path.join(name, "build")

    compilation_task = ninja_task_name(name, binary_name.split("/")[0])

    return {
        "basename": task_name,
        "file_dep": [os.path.join(builds_dir, binary_name)],
        "uptodate": [False],
        "actions": [
            "./tools/sandbox.py {} ./{}/{}".format("" if no_mem_limit else "--no-mem-limit", builds_dir, binary_name)
        ]
    }

def benchmark_task_name(name, binary_name):
    return "bench:{}:{}".format(name, binary_name)


def run_benchmark(name, binary_name):
    task_name = benchmark_task_name(name, binary_name)
    builds_dir = os.path.join(name, "build")

    compilation_task = ninja_task_name(name, binary_name.split("/")[0])
    binary_path = "{}/{}".format(builds_dir, binary_name)
    return {
        "basename": task_name,
        "targets": [binary_path + ".json"],
        "file_dep": [os.path.join(builds_dir, binary_name)],
        "uptodate": [False],
        "actions": [
            "./tools/sandbox.py ./{0} --benchmark_format=json --benchmark_min_time=0.1 > {0}.json".format(binary_path)
        ]
    }


def gen_single_build(name):
    config = yaml.load(open(os.path.join(name, ".tester-config.yaml")))

    gtest_binary = config["gtests"]
    benchmark_binary = config.get("benchmarks")
    
    for build_type in ["release", "debug", "asan", "tsan"]:
        no_mem_limit = build_type in ("asan", "tsan")
        yield gen_cmake(name, build_type)
        yield run_ninja(name, build_type, config)
        yield run_gtest(name, "{}/{}".format(build_type, gtest_binary, no_mem_limit))

        for solution in config.get("solutions", []):
            yield gen_cmake(name, build_type, solution)
            yield run_ninja(name, build_type, config, solution)
            yield run_gtest(name, "{}_{}/{}".format(build_type, solution, gtest_binary, no_mem_limit))

    if benchmark_binary:
        yield run_benchmark(name, "release/{}".format(benchmark_binary))
        for solution in config.get("solutions", []):
            yield run_benchmark(name, "release_{}/{}".format(solution, benchmark_binary))
            

def pre_release_check_single(name):
    config = yaml.load(open(os.path.join(name, ".tester-config.yaml")))

    task_dep = []
    # all our solutions must pass tests
    for build_type in ["release", "asan", "tsan"]:
        for solution in config.get("solutions", []):
            task_dep.append(gtest_task_name(name, "{}_{}/{}".format(build_type, solution, config["gtests"])))

    # reference code must compile
    task_dep.append(ninja_task_name(name, "release"))

    # solution benchmarks should't crash
    if 'benchmarks' in config:
        for solution in config.get("solutions", []):
            task_dep.append(benchmark_task_name(name, "release_{}/{}".format(solution, config["benchmarks"])))

    yield {
        "basename": "pre_release_check:" + name,
        "actions": None,
        "task_dep": task_dep,
    }


def put_score_in_gdoc(name):
    project_name = os.environ["CI_PROJECT_NAME"]
    prefix = "student-"
    if not project_name.startswith(prefix):
        raise ValueError("Strange project name: {}", project_name)
    username = project_name[len(prefix):]
    sheet = course_sheet.get_sheet_from_env()

    task_num = int(name[:2])
    
    now = datetime.datetime.now(TZ_MOSCOW)
    score = 1.0
    if now > TASK_LIST[name]["deadline"]:
        score = 0.3

    sheet.set_score(username, task_num, score)

    
def grade_single(name):
    config = yaml.load(open(os.path.join(name, ".tester-config.yaml")))
    
    task_dep = []

    # tests must pass
    for build_type in ["release", "asan", "tsan"]:
        task_dep.append(gtest_task_name(name, "{}/{}".format(build_type, config["gtests"])))

    # benchmarks should't crash
    if 'benchmarks' in config:
        task_dep.append(benchmark_task_name(name, "release/{}".format(config["benchmarks"])))

    yield {
        "basename": "grade:" + name,
        "actions": [lambda: put_score_in_gdoc(name)],
        "task_dep": task_dep,
    }

def copy_submit_sources(name):
    config = yaml.load(open(os.path.join(name, ".tester-config.yaml")))

    yield {
        "basename": "copy_submit_sources:" + name,
        "actions": [
            "cp $CI_PROJECT_DIR/{name}/{source} {name}/{source}".format(name=name, source=src)
            for src in config.get("allow_change", [])
        ],
        "uptodate": [False]
    }
    pass
    

def task_gen_all():
    """
    Generates actual work in subtask
    """
    for task in TASK_LIST:
        yield gen_single_build(task)
        yield pre_release_check_single(task)
        yield grade_single(task)
        yield copy_submit_sources(task)

def disabled_task_clang_format():
    for task in TASK_LIST:
        files = glob.glob(task + "/*.cpp")
        files += glob.glob(task + "/*.h")
        files += glob.glob("./private/" + task[3:] + "/*.cpp")
        files += glob.glob("./private/" + task[3:] + "/*.h")

        yield {
            'name': task,
            'actions': ['clang-format -i -style=Google ' + ' '.join(files)]
        }
