#pragma once

#include <mutex>
#include <condition_variable>

class EmptyCallback {
public:
    void operator()() {}
};

class Semaphore {
public:
    Semaphore(int count): count_(count) {}

    void leave() {
        std::unique_lock<std::mutex> lock(mutex_);
        ++count_;
        cv_.notify_one();
    }

    template<class Func>
    void enter(Func callback) {
        std::unique_lock<std::mutex> lock(mutex_);
        while (!count_)
            cv_.wait(lock);
        --count_;
        callback();
    }

    void enter() {
        EmptyCallback callback;
        enter(callback);
    }

private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_ = 0;
};

